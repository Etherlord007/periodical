/**
 * 
 */
package ua.kiev.epam.periodical.rdb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * @author dmitriyg 
 * Abstract entity composer class which should be extended to
 *         build entities of some concrete type T.
 *
 */
public abstract class EntityBuilder<T> {

	/**
	 * Constructed entities.
	 */
	protected List<T> entities = new LinkedList<T>();

	/**
	 * This method should be implemented to convert data from ResultSet to
	 * entities of type T.
	 * 
	 * @param rs
	 *            ResultSet to get data from.
	 * @throws SQLException
	 *             if ResultSet error occurs.
	 */
	public abstract void buildEntity(ResultSet rs) throws SQLException;

	/**
	 * Gets first of the entities created using buildEntities() and removes it
	 * from EntityBuilder.
	 * 
	 * @return entity of class T or null if all entities were already fetched.
	 */
	public T getEntity() {
		return (entities.size() > 0) ? entities.remove(0) : null;
	}

	/**
	 * Gets all entities created using buildEntities() method.
	 * 
	 * @return List of entities of class T.
	 */
	public LinkedList<T> getAllEntities() {
		LinkedList<T> result = new LinkedList<T>(entities);
		entities.clear();
		return result;
	}
}
