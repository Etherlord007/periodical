/**
 * 
 */
package ua.kiev.epam.periodical.rdb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kiev.epam.periodical.manager.ConnectionManager;
import ua.kiev.epam.periodical.manager.MessageManager;

/**
 * @author dmitriyg
 * Database Query
 */
public class Query {
	
	private static final Logger logger = LoggerFactory.getLogger(Query.class);
    private static final String DATASOURCE_ERROR = MessageManager.getInstance()
            .getProperty(MessageManager.DATASOURCE_ERROR);
    
    private Connection connection;
    private PreparedStatement statement;

    /**
     * Creates new Query using specified connection.
     *
     * @param connection connection which will be used by this Query.
     */
    public Query(Connection connection) {
        this.connection = connection;
    }

    /**
     * 
     * @param sql 
     *              sql query
     * @param params 
     *              parameters which will be instead "?" in sql query
     * @throws SQLException 
     *              if a database access error occurs.
     */
    private void initPreparedStatement(String sql, Object... params)
            throws SQLException {
        statement = connection.prepareStatement(sql);
        if(params != null){
            for(int i=0; i<params.length; i++){
                statement.setObject(i+1, params[i]);
            }
        }
    }
    
    /**
     * Executes parameterized SELECT statement
     * 
     * @param builder
     *          EntityBuilder which extracts data from ResultSet and use it
     *          to build entities, retrieved using this Query.
     * @param sql
     *          SELECT statement to execute.
     * @param params
     *          SELECT statement parameters.
     * @throws SQLException 
     *          if a database access error occurs.
     */
    public void executeQuery(EntityBuilder<?> builder, String sql,
            Object... params) throws SQLException {
        try{
            initPreparedStatement(sql, params);
            builder.buildEntity(statement.executeQuery());
        } catch(SQLException e){
            logger.error("Error while executing SELECT statement", e);
            throw new SQLException(DATASOURCE_ERROR,e);
        } finally{
            closePreparedStatement();
        }
    }
    
    /**
     * Executes parameterized UPDATE or INSERT statement.
     * 
     * @param sql
     *            UPDATE or INSERT statement to execute.
     * @param params
     *            statement parameters.
     * @return number of rows affected by statement execution.
     * @throws SQLException
     *             if a database access error occurs.
     */
    public int executeUpdate(String sql, Object... params) throws SQLException{
        try{
            initPreparedStatement(sql, params);
            return statement.executeUpdate();
        } catch(SQLException e){
            logger.error("Error while executing UPDATE statement", e);
            throw new SQLException(DATASOURCE_ERROR,e);
        } finally{
            closePreparedStatement();
        }
    }
    
    /**
     * Closes PreparedStatement associated with this Query. Should be invoked
     * after all ResultSets created using this PreparedStatement no longer
     * needed.
     */
    private void closePreparedStatement(){
        try{
            if(statement!=null){
                statement.close();
            }
        }catch (SQLException e){
            logger.warn("Close prepared statement are failed", e);
        }
    }
    
    /**
     * Frees connection.
     * 
     * @param connection
     *                  connection to free.
     */
    public void freeConnection(){
        ConnectionManager.freeConnection(connection);
    }
    
}
