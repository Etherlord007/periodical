/**
 * 
 */
package ua.kiev.epam.periodical.commands.naming;

/**
 * @author dmitriyg
 * Contains request parameter names
 */
public class RequestAttribute {
	
	public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String PASSWORD = "password";
    public static final String ON_ERROR_GO_PATH = "on_error_go_path";
    public static final String MESSAGE = "message";
    public static final String NEW_LOCALE = "new_locale";
    public static final String SUBSCRIPTION = "subscription";
    public static final String SERIAL_NAME = "serial_name";
    public static final String SERIAL_PERIOD = "serial_period";
    public static final String SERIAL_DETAILS = "serial_details";
    public static final String SERIAL_PRICE = "serial_price_per_mounth";
    public static final String SERIAL_IMAGE = "serial_image_path";
    

}
