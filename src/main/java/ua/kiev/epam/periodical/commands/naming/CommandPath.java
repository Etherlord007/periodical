/**
 * 
 */
package ua.kiev.epam.periodical.commands.naming;

/**
 * @author dmitriyg
 * Containing action paths, which are used to invoke chain of actions.
 */
public class CommandPath {
	
	public static final String HOME = "home";
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";

}
