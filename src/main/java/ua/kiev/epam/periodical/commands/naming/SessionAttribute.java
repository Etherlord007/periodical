/**
 * 
 */
package ua.kiev.epam.periodical.commands.naming;

/**
 * @author dmitriyg 
 * Containing session parameter names
 *
 */
public class SessionAttribute {

	public static final String USER = "user";
	public static final String LOCALE = "locale";
	public static final String CART = "cart";

}
