/**
 * 
 */
package ua.kiev.epam.periodical.tags;

import java.io.IOException;
import java.util.Date;
import java.text.DateFormat;
import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * @author dmitriyg
 *
 */
public class DateTimeTag extends TagSupport{
	
	private static final long serialVersionUID = -8390134470175811085L;
	
	private Locale locale;
	private Integer style;
	
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	
	public void setStyle(Integer style) {
		this.style = style;
	}

	@Override
	public int doStartTag() throws JspException {
		try {
			DateFormat dateFormat = DateFormat.getDateTimeInstance(style,
						style, locale);
			pageContext.getOut().write(dateFormat.format(new Date()));
		} catch (IOException e) {
			throw new JspException(e);
		}

		return SKIP_BODY;
	}
}
