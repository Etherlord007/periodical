/**
 * 
 */
package ua.kiev.epam.periodical.controller;

import javax.servlet.http.HttpServletRequest;

import ua.kiev.epam.periodical.command.Command;
import ua.kiev.epam.periodical.command.ValidCommand;

/**
 * @author dmitriyg
 *
 */
public class RequestHelper {
	/** The Constant INSTANCE. */
    private static volatile RequestHelper INSTANCE = new RequestHelper();

    /**
     * Instantiates a new request helper.
     */
    private RequestHelper() {
    }

    /**
     * Get RequestHelper instance.
     *
     * @return RequestHelper instance.
     */
    public static RequestHelper getInstance() {
    	RequestHelper localInstance = INSTANCE;
        if (localInstance == null) {
            synchronized (RequestHelper.class) {
                localInstance = INSTANCE;
                if (localInstance == null) {
                	INSTANCE = localInstance = new RequestHelper();
                }
            }
        }
        return localInstance;
    }

    /**
     * Gets action for specified request using request URL.
     *
     * @param req request to get action for.
     * @return action which should be invoked for this request.
     */
    public Command getCommand(HttpServletRequest req) {

        String actionName = req.getServletPath(); 
        actionName = actionName.substring(actionName.lastIndexOf('/') + 1);
        return ValidCommand.valueOf(actionName.toUpperCase()).getCommand();
    }
}
