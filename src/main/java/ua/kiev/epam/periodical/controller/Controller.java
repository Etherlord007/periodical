/**
 * 
 */
package ua.kiev.epam.periodical.controller;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kiev.epam.periodical.command.Command;
import ua.kiev.epam.periodical.commands.naming.CommandPath;
import ua.kiev.epam.periodical.commands.naming.RequestAttribute;
import ua.kiev.epam.periodical.dao.DaoFactory;
import ua.kiev.epam.periodical.manager.ConfigurationManager;
import ua.kiev.epam.periodical.manager.ConnectionManager;

/**
 * @author dmitriyg
 *
 */
@WebServlet(name = "Controller", urlPatterns = {"/home", "/login", "/logout" , "/change_locale","/add_serial_in_cart"})
public class Controller extends HttpServlet{

	private static final long serialVersionUID = -2583581541136080862L;
	private static final Logger logger = LoggerFactory.getLogger(Controller.class);
	
	@Override
    public void init() throws ServletException {
        try {
            DaoFactory.initialize(DaoFactory.MYSQL);
            ConnectionManager.initilize();
        } catch (NamingException e) {
            logger.error(
                    "Servlet couldn't be initialized: failed to initialize connection pool.",
                    e);
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //определение команды, пришедшей из JSP
        Command command = RequestHelper.getInstance().getCommand(request);

        try {
            command.execute(request, response);
        } catch (Exception e) {
            request.setAttribute(RequestAttribute.MESSAGE, e.getMessage());
            request.setAttribute(RequestAttribute.ON_ERROR_GO_PATH, CommandPath.HOME);
            request.getRequestDispatcher(ConfigurationManager.getInstance()
                    .getProperty(ConfigurationManager.ERROR_PAGE_PATH))
                    .forward(request, response);
        }

    }
}
