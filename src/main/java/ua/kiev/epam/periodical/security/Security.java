/**
 * 
 */
package ua.kiev.epam.periodical.security;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kiev.epam.periodical.commands.naming.SessionAttribute;
import ua.kiev.epam.periodical.entities.User;

/**
 * @author dmitriyg
 * Controls user access to requested resources
 */
public class Security {
	
	private static final Logger logger = LoggerFactory.getLogger(Security.class);

	/**
	 * This method should be called from every action which is not publicly
	 * accessible to control whether current user has rights for such a request.
	 * 
	 * @param request user request to resource.
	 * @param requiedRole
	 *            Role requied to access resource.
	 * @throws SecurityException
	 *             if there's a try to access some resources without
	 *             permissions.
	 */
    
        public static void ensure(HttpServletRequest request, Role requiedRole)
                    throws SecurityException{
            User user = (User) request.getSession().getAttribute(SessionAttribute.USER);
        if ((user == null) || (requiedRole != Role.valueOf(user.getRole().toUpperCase()))) {
            logger.warn("Unpermitted access to resource: " + request.getRequestURL()
                        +" from address " + request.getRemoteAddr() + ".");
            throw new SecurityException("You don't have permissions to view this page.");
        }
    }
}
