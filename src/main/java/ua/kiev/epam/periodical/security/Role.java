/**
 * 
 */
package ua.kiev.epam.periodical.security;

/**
 * @author dmitriyg
 *
 */
public enum Role {
	
	UNREGISTERED_USER, USER, ADMIN, INACTIVE
}
