/**
 * 
 */
package ua.kiev.epam.periodical.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kiev.epam.periodical.dao.UserDao;
import ua.kiev.epam.periodical.entities.User;
import ua.kiev.epam.periodical.rdb.EntityBuilder;

/**
 * @author dmitriyg
 *
 */
public class UserDaoMySQL extends DaoMySQL implements UserDao {

	private static final Logger logger= LoggerFactory
			.getLogger(UserDaoMySQL.class);

	private static final String SELECT_ALL_USER = "SELECT * FROM User";
	private static final String SELECT_USER_BY_ID = "SELECT * FROM User WHERE id = ?";
	private static final String SELECT_USER_BY_LOGIN_AND_PASS = "SELECT * FROM User WHERE login = ? AND passwordHash = ?";
	// private static final String SELECT_BY_ROLE =
	// "SELECT * FROM user WHERE role = ?";
	private static final String INSERT_NEW_USER = "INSERT INTO User VALUES (NULL, ?, ?, ?)";
	private static final String UPDATE_USER = "UPDATE User SET login = ? AND passwordHash = ? AND role = ?";
	private static final String DELETE_USER_BY_LOGIN = "DELETE FROM User WHERE login = ?";
	private static final String DELETE_USER_BY_ID = "DELETE FROM User WHERE id = ?";

	public UserDaoMySQL() throws SQLException {
	}

	private EntityBuilder<User> builder = new EntityBuilder<User>() {

		private static final int USER_ID_COLUMN = 1;
		private static final int LOGIN_COLUMN = 2;
		private static final int PASSWORD_COLUMN = 3;
		private static final int ROLE_COLUMN = 4;

		@Override
		public void buildEntity(ResultSet rs) throws SQLException {
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt(USER_ID_COLUMN));
				user.setLogin(rs.getString(LOGIN_COLUMN));
				user.setPasswordHash(rs.getString(PASSWORD_COLUMN));
				user.setRole(rs.getString(ROLE_COLUMN));
				entities.add(user);
			}
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#findById(int)
	 */
	@Override
	public User findById(int id) throws SQLException {
		try {
			query.executeQuery(builder, SELECT_USER_BY_ID, id);
			return builder.getEntity();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#findAll(java.lang.Object)
	 */
	@Override
	public LinkedList<User> findAll() throws SQLException {
		try {
			query.executeQuery(builder, SELECT_ALL_USER);
			return builder.getAllEntities();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#save(java.lang.Object)
	 */
	@Override
	public boolean save(User entity) throws SQLException {
		try {
			int result = query.executeUpdate(INSERT_NEW_USER,
					entity.getLogin(), entity.getPasswordHash(),
					entity.getRole());
			return (result > 0);
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#update(java.lang.Object)
	 */
	@Override
	public User update(User entity) throws SQLException {
		try {
			User r = this.findById(entity.getId());
			query.executeUpdate(UPDATE_USER, entity.getLogin(),
					entity.getPasswordHash(), entity.getRole());
			return r;
		} catch (SQLException e) {
			logger.error("Execute SQL statement was failed", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(User entity) throws SQLException {
		try {
			int result = query.executeUpdate(DELETE_USER_BY_ID, entity.getId());
			return (result > 0);
		} catch (SQLException e) {
			logger.error("Execute SQL statement was failed", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.UserDao#getUser(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public User getUser(String login, String password) throws SQLException {
		try {
			query.executeQuery(builder, SELECT_USER_BY_LOGIN_AND_PASS, login,
					password);
			return builder.getEntity();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.UserDao#deleteByLogin(java.lang.String)
	 */
	@Override
	public boolean deleteByLogin(String login) throws SQLException {
		try {
			int result = query.executeUpdate(DELETE_USER_BY_LOGIN, login);
			return (result > 0);
		} catch (SQLException e) {
			logger.error("Execute SQL statement was failed", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

}
