/**
 * 
 */
package ua.kiev.epam.periodical.dao;

import java.sql.SQLException;
import java.util.List;

import ua.kiev.epam.periodical.entities.Serial;

/**
 * @author dmitriyg
 *
 */
public interface SerialDao extends EntityDao<Serial> {
	
	/**
	 * Finds serial by its name
	 * 
	 * @param name
	 * 		Serial name
	 * @return
	 * 		Serial
	 * @throws SQLException
	 */
	Serial findByName(String name) throws SQLException;
	
	/**
	 * Finds list of serial what has period less then $period
	 * 
	 * @param period
	 * 		Serial period
	 * @return
	 * 		List of serials
	 * @throws SQLException
	 */
	List<Serial> findByPeriod(int period) throws SQLException;
	
	boolean deleteByName(String name) throws SQLException;
	
	List<Serial> findSerialsBySub(int id) throws SQLException;
	
	boolean insertWithSet(Serial entity) throws SQLException;
}
