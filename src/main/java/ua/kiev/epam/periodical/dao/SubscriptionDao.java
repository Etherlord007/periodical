/**
 * 
 */
package ua.kiev.epam.periodical.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import ua.kiev.epam.periodical.entities.Subscription;

/**
 * @author dmitriyg
 *
 */
public interface SubscriptionDao extends EntityDao<Subscription> {
	
	/**
	 * Finds all Subscriptions for selected userId
	 * 
	 * @param userId
	 * 		User id
	 * @return
	 * 		List of Subscriptions
	 */
	
	List <Subscription> findSubsByDate(Date date) throws SQLException;
	
	List <Subscription> findSubsByUser(Integer id) throws SQLException;
	
	boolean insertWithSet(Subscription entity) throws SQLException;
}
