/**
 * 
 */
package ua.kiev.epam.periodical.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kiev.epam.periodical.dao.SerialDao;
import ua.kiev.epam.periodical.entities.Serial;
import ua.kiev.epam.periodical.rdb.EntityBuilder;

/**
 * @author dmitriyg
 *
 */
public class SerialDaoMySQL extends DaoMySQL implements SerialDao {

	private static final Logger logger = LoggerFactory
			.getLogger(SerialDaoMySQL.class);

	private static final String SELECT_ALL_SERIAL = "SELECT * FROM Serial";
	private static final String SELECT_SERIAL_BY_ID = "SELECT * FROM Serial WHERE id = ?";
	private static final String SELECT_SERIAL_BY_SUBSCRIPTION_ID = "SELECT * FROM Serial JOIN SubToSerial ON Serial.id = SubToSerial.serialId WHERE SubToSerial.subscriptionId = ?";
	private static final String SELECT_SERIAL_BY_NAME = "SELECT * FROM Serial WHERE name = ?";
	private static final String SELECT_SERIAL_BY_PERIOD = "SELECT * FROM Serial WHERE period < ?";
	private static final String INSERT_NEW_SERIAL = "INSERT INTO Serial VALUES (NULL, ?, ?, ?, ?, ?)";
	private static final String INSERT_NEW_SERIAL_WITH_SET = "ISERT IGNORE INTO Serial VALUES (NULL, ?, ?, ?, ?, ?); SET @serialId = last_insert_id(); INSERT into periodical.SubToSerial values (@periodicalId,@serialId)";
	private static final String UPDATE_SERIAL = "UPDATE Serial SET name = ? AND period = ? AND details = ? AND priсePerMounth = ? AND imagePath = ?";
	private static final String DELETE_SERIAL_BY_NAME = "DELETE FROM Serial WHERE name = ?";
	private static final String DELETE_SERIAL_BY_ID = "DELETE FROM Serial WHERE id = ?";

	public SerialDaoMySQL() throws SQLException {
	}

	private EntityBuilder<Serial> builder = new EntityBuilder<Serial>() {

		private static final int SERIAL_ID_COLUMN = 1;
		private static final int NAME_COLUMN = 2;
		private static final int PERIOD_COLUMN = 3;
		private static final int DETAILS_COLUMN = 4;
		private static final int PRICE_PER_MOUNTH_COLUMN = 5;
		private static final int IMAGE_PATH_COLUMN = 6;

		@Override
		public void buildEntity(ResultSet rs) throws SQLException {
			while (rs.next()) {
				Serial serial = new Serial();
				serial.setId(rs.getInt(SERIAL_ID_COLUMN));
				serial.setName(rs.getString(NAME_COLUMN));
				serial.setPeriod(rs.getInt(PERIOD_COLUMN));
				serial.setDetails(rs.getString(DETAILS_COLUMN));
				serial.setPricePerMonth(rs.getDouble(PRICE_PER_MOUNTH_COLUMN));
				serial.setImage(rs.getString(IMAGE_PATH_COLUMN));
				entities.add(serial);
			}
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#findById(int)
	 */
	@Override
	public Serial findById(int id) throws SQLException {
		try {
			query.executeQuery(builder, SELECT_SERIAL_BY_ID, id);
			return builder.getEntity();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#findAll()
	 */
	@Override
	public LinkedList<Serial> findAll() throws SQLException {
		try {
			query.executeQuery(builder, SELECT_ALL_SERIAL);
			return builder.getAllEntities();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#save(java.lang.Object)
	 */
	@Override
	public boolean save(Serial entity) throws SQLException {
		try {
			int result = query.executeUpdate(INSERT_NEW_SERIAL,
					entity.getName(), entity.getPeriod(), entity.getDetails(),
					entity.getPricePerMonth(), entity.getImage());
			return (result > 0);
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#update(java.lang.Object)
	 */
	@Override
	public Serial update(Serial entity) throws SQLException {
		try{
			Serial r = this.findById(entity.getId());
			query.executeUpdate(UPDATE_SERIAL,entity.getName(), entity.getPeriod(), entity.getDetails(),
					entity.getPricePerMonth(), entity.getImage());
			return r;
		}
		catch(SQLException e){
			logger.error("Execute SQL statement was failed", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Serial entity) throws SQLException {
		try{
            int result = query.executeUpdate(DELETE_SERIAL_BY_ID, entity.getId());
            return (result > 0);
        }catch(SQLException e){
            logger.error("Execute SQL statement was failed", e);
            throw new SQLException(DATASOURCE_ERROR, e);
        }
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.SerialDao#findByName(java.lang.String)
	 */
	@Override
	public Serial findByName(String name) throws SQLException {
		try {
			query.executeQuery(builder, SELECT_SERIAL_BY_NAME, name);
			return builder.getEntity();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.SerialDao#findByPeriod(int)
	 */
	@Override
	public List<Serial> findByPeriod(int period) throws SQLException {
		try {
			query.executeQuery(builder, SELECT_SERIAL_BY_PERIOD, period);
			return builder.getAllEntities();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/* (non-Javadoc)
	 * @see ua.kiev.epam.periodical.dao.SerialDao#deleteById(int)
	 */
	@Override
	public boolean deleteByName(String name) throws SQLException {
		try{
            int result = query.executeUpdate(DELETE_SERIAL_BY_NAME, name);
            return (result > 0);
        }catch(SQLException e){
            logger.error("Execute SQL statement was failed", e);
            throw new SQLException(DATASOURCE_ERROR, e);
        }
	}

	/* (non-Javadoc)
	 * @see ua.kiev.epam.periodical.dao.SerialDao#findSerialsBySub(int)
	 */
	@Override
	public List<Serial> findSerialsBySub(int id) throws SQLException{
		try {
			query.executeQuery(builder, SELECT_SERIAL_BY_SUBSCRIPTION_ID, id);
			return builder.getAllEntities();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/* (non-Javadoc)
	 * @see ua.kiev.epam.periodical.dao.SerialDao#insertWithSet(ua.kiev.epam.periodical.entities.Serial)
	 */
	@Override
	public boolean insertWithSet(Serial entity) throws SQLException {
		try {
			int result = query.executeUpdate(INSERT_NEW_SERIAL_WITH_SET,
					entity.getName(), entity.getPeriod(), entity.getDetails(),
					entity.getPricePerMonth(), entity.getImage());
			return (result > 0);
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

}
