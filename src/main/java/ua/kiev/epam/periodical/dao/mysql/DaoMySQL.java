/**
 * 
 */
package ua.kiev.epam.periodical.dao.mysql;

import java.sql.SQLException;

import ua.kiev.epam.periodical.manager.ConnectionManager;
import ua.kiev.epam.periodical.manager.MessageManager;
import ua.kiev.epam.periodical.rdb.Query;

/**
 * @author dmitriyg
 *
 */
public class DaoMySQL implements AutoCloseable{
	
	protected static final String DATASOURCE_ERROR = MessageManager
			.getInstance().getProperty(MessageManager.DATASOURCE_ERROR);
    
    Query query;
    
    DaoMySQL()throws SQLException {
        query = new Query(ConnectionManager.getConnection());
    }
    
    /**
     * Closes connection associated with this DAO.
     */
    public void close() {
        query.freeConnection();
    }
}
