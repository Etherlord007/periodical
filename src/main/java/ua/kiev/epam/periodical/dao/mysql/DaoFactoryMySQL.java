/**
 * 
 */
package ua.kiev.epam.periodical.dao.mysql;

import ua.kiev.epam.periodical.dao.DaoFactory;
import ua.kiev.epam.periodical.dao.SerialDao;
import ua.kiev.epam.periodical.dao.SubscriptionDao;
import ua.kiev.epam.periodical.dao.UserDao;

/**
 * @author dmitriyg
 *
 */
public class DaoFactoryMySQL extends DaoFactory {

	/* (non-Javadoc)
	 * @see ua.kiev.epam.periodical.dao.DaoFactory#getUserDao()
	 */
	@Override
	public UserDao getUserDao() throws Exception {
		return new UserDaoMySQL();
	}

	/* (non-Javadoc)
	 * @see ua.kiev.epam.periodical.dao.DaoFactory#getSerialDao()
	 */
	@Override
	public SerialDao getSerialDao() throws Exception {
		return new SerialDaoMySQL();
	}

	/* (non-Javadoc)
	 * @see ua.kiev.epam.periodical.dao.DaoFactory#getSubscriptionDao()
	 */
	@Override
	public SubscriptionDao getSubscriptionDao() throws Exception {
		return new SubscriptionDaoMySQL();
	}

}
