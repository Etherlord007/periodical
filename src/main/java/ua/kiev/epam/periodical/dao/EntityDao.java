/**
 * 
 */
package ua.kiev.epam.periodical.dao;

import java.sql.SQLException;
import java.util.List;

/**
 * Basic DAO for all entities. Includes all crowd methods
 * 
 * @author dmitriyg
 */
public interface EntityDao<T> extends AutoCloseable{

	/**
	 * Find entity by its id
	 * 
	 * @param id
	 *            Entity id
	 * @return Entity
	 */
	T findById(int id) throws SQLException;

	/**
	 * Find all entities of selected type
	 * 
	 * @param entityType
	 *            Entity type
	 * @return List of entities
	 */
	List<T> findAll() throws SQLException;

	/**
	 * Saves new Entity
	 *
	 * @param entity
	 *            New entity
	 * @param <T>
	 *            Entity type
	 */
	boolean save(T entity) throws SQLException;

	/**
	 * Updates data from persistent storage
	 *
	 * @param entity
	 *            Entity
	 * @param <T>
	 *            Entity type
	 * @return Persisted entity after update
	 */
	T update(T entity) throws SQLException;

	/**
	 * Delete entity from persistent storage
	 *
	 * @param entity
	 *            Entity
	 * @param <T>
	 *            Entity type
	 */
	boolean delete(T entity) throws SQLException;


}
