/**
 * 
 */
package ua.kiev.epam.periodical.dao;

import ua.kiev.epam.periodical.dao.mysql.DaoFactoryMySQL;



/**
 * Abstract DAO factory. This class should be used to retrieve concrete
 * factories and instantiate DAOs.
 * @author dmitriyg
 *
 */
public abstract class DaoFactory {
	
	public static final int MYSQL = 1;
    public static final int ORACLE = 2;
    private static int concreteFactoryType;
    
    /**
     * Initializes DAO factory to work with specific data source type currently
     * used.
     *
     * @param factoryType factory type to init with (use DaoFactory static
     *                    fields to find out which factory types are avaliable).
     */
    public static void initialize(int factoryType){
        concreteFactoryType = factoryType; 
    }
    
    /**
     * 
     * @return new object dao factoryMySQL type
     */
    public static DaoFactory getFactory() throws ClassNotFoundException{ 
        switch(concreteFactoryType){
            case MYSQL: 
                        return new DaoFactoryMySQL();
            case ORACLE:
                        throw new ClassNotFoundException("Unknown DB type: " 
                                                         + concreteFactoryType);
            default:
                    throw new IllegalStateException("Dao Factory wasn't initialized "
					+ "or was initialized unappropriate.");
        }
    }
    
    public abstract UserDao getUserDao() throws Exception;
    
    public abstract SerialDao getSerialDao() throws Exception;
    
    public abstract SubscriptionDao getSubscriptionDao() throws Exception;

}
