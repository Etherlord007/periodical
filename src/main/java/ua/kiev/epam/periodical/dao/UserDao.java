/**
 * 
 */
package ua.kiev.epam.periodical.dao;

import java.sql.SQLException;

import ua.kiev.epam.periodical.entities.User;

/**
 * @author dmitriyg
 *
 */
public interface UserDao extends EntityDao<User> {
	
	/**
	 * Find User by its Login and Password
	 * 
	 * @param login
	 * 		User login
	 * @param password
	 * 		User password
	 * @return
	 * 		User
	 * @throws SQLException
	 */
	User getUser(String login, String password) throws SQLException;
	
	boolean deleteByLogin(String login) throws SQLException;
}
