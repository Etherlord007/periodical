/**
 * 
 */
package ua.kiev.epam.periodical.dao.mysql;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kiev.epam.periodical.dao.SubscriptionDao;
import ua.kiev.epam.periodical.entities.Subscription;
import ua.kiev.epam.periodical.rdb.EntityBuilder;

/**
 * @author dmitriyg
 *
 */
public class SubscriptionDaoMySQL extends DaoMySQL implements SubscriptionDao {

	private static final Logger logger = LoggerFactory
			.getLogger(SubscriptionDaoMySQL.class);

	private static final String SELECT_ALL_SUBSCRIPTION = "SELECT * FROM Subscription";
	private static final String SELECT_SUBSCRIPTION_BY_ID = "SELECT * FROM Subscription WHERE id = ?";
	private static final String SELECT_SUBSCRIPTION_BY_USER_ID = "SELECT * FROM Subscription WHERE userId = ?";
	private static final String SELECT_SUBSCRIPTION_BY_DATE = "SELECT * FROM Subscription WHERE date = ?";
	private static final String INSERT_NEW_SUBSCRIPTION = "INSERT INTO Subscription VALUES (NULL, ?, ?, ?)";
	private static final String INSERT_NEW_SUBSCRIPTION_WITH_SET = "INSERT INTO Subscription VALUES (NULL, ?, ?, ?); SET @periodicalId = last_insert_id()";
	private static final String UPDATE_SUBSCRIPTION = "UPDATE Subscription SET paymentId = ? AND totalSum = ? AND userId = ?";
	private static final String DELETE_SUBSCRIPTION_BY_ID = "DELETE FROM Subscription WHERE subscriptionId = ?";

	SubscriptionDaoMySQL() throws SQLException {
	}

	private EntityBuilder<Subscription> builder = new EntityBuilder<Subscription>() {

		private static final int SUBSCRIPTION_ID_COLUMN = 1;
		private static final int TOTAL_SUM_COLUMN = 2;
		private static final int SUBSCRIPTION_DATE_COLUMN = 3;
		private static final int USER_ID_COLUMN = 4;

		@Override
		public void buildEntity(ResultSet rs) throws SQLException {
			while (rs.next()) {
				Subscription sub = new Subscription();
				sub.setId(rs.getInt(SUBSCRIPTION_ID_COLUMN));
				sub.setTotalSum(rs.getDouble(TOTAL_SUM_COLUMN));
				sub.setDateOfSub(rs.getDate(SUBSCRIPTION_DATE_COLUMN));
				sub.setUserId(rs.getInt(USER_ID_COLUMN));
				entities.add(sub);
			}
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#findById(int)
	 */
	@Override
	public Subscription findById(int id) throws SQLException {
		try {
			query.executeQuery(builder, SELECT_SUBSCRIPTION_BY_ID, id);
			return builder.getEntity();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#findAll()
	 */
	@Override
	public LinkedList<Subscription> findAll() throws SQLException {
		try {
			query.executeQuery(builder, SELECT_ALL_SUBSCRIPTION);
			return builder.getAllEntities();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#save(java.lang.Object)
	 */
	@Override
	public boolean save(Subscription entity) throws SQLException {
		try {
			int result = query.executeUpdate(INSERT_NEW_SUBSCRIPTION,
					entity.getTotalSum(), entity.getDateOfSub(), entity.getUserId());
			return (result > 0);
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#update(java.lang.Object)
	 */
	@Override
	public Subscription update(Subscription entity) throws SQLException {
		try {
			Subscription r = this.findById(entity.getId());
			query.executeUpdate(UPDATE_SUBSCRIPTION, entity.getTotalSum(),
					entity.getDateOfSub(), entity.getUserId());
			return r;
		} catch (SQLException e) {
			logger.error("Execute SQL statement was failed", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.dao.EntityDao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Subscription entity) throws SQLException {
		try {
			Subscription u = (Subscription) entity;
			int result = query.executeUpdate(DELETE_SUBSCRIPTION_BY_ID,
					u.getId());
			return (result > 0);
		} catch (SQLException e) {
			logger.error("Execute SQL statement was failed", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.kiev.epam.periodical.dao.SubscriptionDao#findSubsByDate(java.sql.Date)
	 */
	@Override
	public List<Subscription> findSubsByDate(Date date) throws SQLException {
		try {
			query.executeQuery(builder, SELECT_SUBSCRIPTION_BY_DATE, date);
			return builder.getAllEntities();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/* (non-Javadoc)
	 * @see ua.kiev.epam.periodical.dao.SubscriptionDao#findSubsByUser(java.lang.Integer)
	 */
	@Override
	public List<Subscription> findSubsByUser(Integer id) throws SQLException {
		try {
			query.executeQuery(builder, SELECT_SUBSCRIPTION_BY_USER_ID, id);
			return builder.getAllEntities();
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/* (non-Javadoc)
	 * @see ua.kiev.epam.periodical.dao.SubscriptionDao#insertWithSet(ua.kiev.epam.periodical.entities.Subscription)
	 */
	@Override
	public boolean insertWithSet(Subscription entity) throws SQLException {
		try {
			int result = query.executeUpdate(INSERT_NEW_SUBSCRIPTION_WITH_SET,
					entity.getTotalSum(), entity.getDateOfSub(), entity.getUserId());
			return (result > 0);
		} catch (SQLException e) {
			logger.error("Execute SQL statement was faild", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

}
