/**
 * 
 */
package ua.kiev.epam.periodical.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import ua.kiev.epam.periodical.manager.ConfigurationManager;

/**
 * @author dmitriyg
 *
 */
//@WebFilter(urlPatterns = {"/*"})
public class EncodingFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		String encoding = ConfigurationManager.getInstance().getProperty(
				ConfigurationManager.DEFAULT_ENCODING);
		if (!encoding.equalsIgnoreCase(req.getCharacterEncoding())) {
			req.setCharacterEncoding(encoding);
		}

		if (chain != null) {
			chain.doFilter(req, resp);
		}
	}

}
