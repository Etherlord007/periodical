/**
 * 
 */
package ua.kiev.epam.periodical.filter;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ua.kiev.epam.periodical.commands.naming.SessionAttribute;
import ua.kiev.epam.periodical.manager.ConfigurationManager;

/**
 * @author dmitriyg
 *
 */
//@WebFilter(urlPatterns = {"/*"})
public class LocaleFilter implements Filter {

	@Override
	public void init(FilterConfig fc) throws ServletException {
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpSession session = request.getSession();
		Locale locale = (Locale) session.getAttribute(SessionAttribute.LOCALE);

		if (locale == null) {
			String language = ConfigurationManager.getInstance().getProperty(
					ConfigurationManager.DEFAULT_LANGUAGE);
			session.setAttribute(SessionAttribute.LOCALE, new Locale(language));
		}

		if (chain != null) {
			chain.doFilter(request, resp);
		}
	}

}
