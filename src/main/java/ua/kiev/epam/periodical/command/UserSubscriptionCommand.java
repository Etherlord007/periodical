/**
 * 
 */
package ua.kiev.epam.periodical.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kiev.epam.periodical.commands.naming.CommandPath;
import ua.kiev.epam.periodical.commands.naming.RequestAttribute;
import ua.kiev.epam.periodical.commands.naming.SessionAttribute;
import ua.kiev.epam.periodical.dao.DaoFactory;
import ua.kiev.epam.periodical.dao.SerialDao;
import ua.kiev.epam.periodical.dao.SubscriptionDao;
import ua.kiev.epam.periodical.entities.Subscription;
import ua.kiev.epam.periodical.entities.User;
import ua.kiev.epam.periodical.manager.ConfigurationManager;
import ua.kiev.epam.periodical.manager.MessageManager;

/**
 * @author dmitriyg
 *
 */
public class UserSubscriptionCommand implements Command {
	private static final Logger logger = LoggerFactory
			.getLogger(UserSubscriptionCommand.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.command.Command#execute(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try (SubscriptionDao subDao = DaoFactory.getFactory().getSubscriptionDao();
				SerialDao serialDao = DaoFactory.getFactory().getSerialDao()){
			User user = ((User) request.getSession().getAttribute(
					SessionAttribute.USER));
			if (user != null) {
				List<Subscription> ls = subDao.findSubsByUser(user.getId());

				request.setAttribute(RequestAttribute.SUBSCRIPTION, ls);
				request.getRequestDispatcher(
						ConfigurationManager.getInstance().getProperty(
								ConfigurationManager.SUBSCRIPTION_PAGE_PATH))
						.forward(request, response);
				logger.info("User :" + user.getLogin()
						+ " view his Subscription");
			} else {
				request.setAttribute(RequestAttribute.MESSAGE,
						MessageManager.YOU_ARE_NOT_LOGGED_IN);
				request.setAttribute(RequestAttribute.ON_ERROR_GO_PATH,
						CommandPath.HOME);
				request.getRequestDispatcher(
						ConfigurationManager.getInstance().getProperty(
								ConfigurationManager.ERROR_PAGE_PATH)).forward(
						request, response);
				logger.info("Unsuccessfull check out : User was not logged in");
			}
		}
	}

}
