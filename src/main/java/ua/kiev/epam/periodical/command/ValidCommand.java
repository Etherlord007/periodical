package ua.kiev.epam.periodical.command;

/**
 * 
 */

/**
 * @author dmitriyg
 *
 */
public enum ValidCommand {
	
	NO_COMMAND(new NoCommand()),
    HOME(new HomeCommand()),
    LOGIN(new LoginCommand()),
    LOGOUT(new LogoutCommand()),
    CHANGE_LOCALE(new ChangeLocaleCommand()),
    ADD_SERIAL_IN_CART(new AddSerialInCart()),
    USER_SUBSCRIPTION(new UserSubscriptionCommand()),
	CHECK_OUT(new CheckOutCommand());
	
public Command command;
    
    ValidCommand (Command command){
        this.command = command;
    }
    
    public Command getCommand(){
        return command;
    }
}
