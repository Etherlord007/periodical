/**
 * 
 */
package ua.kiev.epam.periodical.command;

import java.sql.Date;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kiev.epam.periodical.commands.naming.CommandPath;
import ua.kiev.epam.periodical.commands.naming.RequestAttribute;
import ua.kiev.epam.periodical.commands.naming.SessionAttribute;
import ua.kiev.epam.periodical.converters.DateTimeConverter;
import ua.kiev.epam.periodical.dao.DaoFactory;
import ua.kiev.epam.periodical.dao.SerialDao;
import ua.kiev.epam.periodical.dao.SubscriptionDao;
import ua.kiev.epam.periodical.entities.Serial;
import ua.kiev.epam.periodical.entities.Subscription;
import ua.kiev.epam.periodical.entities.User;
import ua.kiev.epam.periodical.manager.ConfigurationManager;
import ua.kiev.epam.periodical.manager.MessageManager;

/**
 * @author dmitriyg
 *
 */
public class CheckOutCommand implements Command {

	private static final Logger logger = LoggerFactory
			.getLogger(LoginCommand.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.command.Command#execute(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try (SubscriptionDao subDao = DaoFactory.getFactory().getSubscriptionDao();
				SerialDao serialDao = DaoFactory.getFactory().getSerialDao()){			
			User user = ((User) request.getSession().getAttribute(
					SessionAttribute.USER));
			if (user == null) {
				request.setAttribute(RequestAttribute.MESSAGE,
						MessageManager.YOU_ARE_NOT_LOGGED_IN);
				request.setAttribute(RequestAttribute.ON_ERROR_GO_PATH,
						CommandPath.HOME);
				request.getRequestDispatcher(
						ConfigurationManager.getInstance().getProperty(
								ConfigurationManager.ERROR_PAGE_PATH)).forward(
						request, response);
				logger.info("Unsuccessfull check out : User was not logged in");
			} else {
				@SuppressWarnings("unchecked")
				LinkedList<Serial> cart = new LinkedList<>((LinkedList<Serial>) request
						.getSession().getAttribute(SessionAttribute.CART));
				if(cart.isEmpty()){
					request.setAttribute(RequestAttribute.MESSAGE,
							MessageManager.YOUR_CART_IS_EMPTY);
					request.setAttribute(RequestAttribute.ON_ERROR_GO_PATH,
							CommandPath.HOME);
					request.getRequestDispatcher(
							ConfigurationManager.getInstance().getProperty(
									ConfigurationManager.ERROR_PAGE_PATH)).forward(
							request, response);
					logger.info("Unsuccessfull check out : User has empty cart");
				}
				else{
					Double totalSum = 0.0;
					for (Serial s : cart){
						totalSum += s.getPeriod() * s.getPricePerMonth();
					}
					DateTimeConverter d = new DateTimeConverter();
					Date date = d.convertToDatabaseColumn(new DateTime());
					Subscription s = new Subscription(null, totalSum, date, user.getId());
					synchronized(this) {
						subDao.insertWithSet(s);
						for (Serial se : cart){
							serialDao.insertWithSet(se);
						}
					}
				}
			}
		}
	}
}
