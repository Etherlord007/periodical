/**
 * 
 */
package ua.kiev.epam.periodical.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kiev.epam.periodical.commands.naming.CommandPath;
import ua.kiev.epam.periodical.commands.naming.RequestAttribute;
import ua.kiev.epam.periodical.manager.ConfigurationManager;
import ua.kiev.epam.periodical.manager.MessageManager;

/**
 * @author dmitriyg
 *
 */
public class NoCommand implements Command {

	private static final Logger logger = LoggerFactory
			.getLogger(NoCommand.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.command.Command#execute(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		request.setAttribute(RequestAttribute.MESSAGE, MessageManager
				.getInstance().getProperty(MessageManager.PAGE_NOT_FOUND_ERROR));
		request.setAttribute(RequestAttribute.ON_ERROR_GO_PATH,
				CommandPath.HOME);
		request.getRequestDispatcher(
				ConfigurationManager.getInstance().getProperty(
						ConfigurationManager.ERROR_PAGE_PATH)).forward(request,
				response);
		logger.info("Page not found: " + request.getRequestURL() + ".");
	}

}
