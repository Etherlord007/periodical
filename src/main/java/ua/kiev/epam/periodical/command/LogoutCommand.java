/**
 * 
 */
package ua.kiev.epam.periodical.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kiev.epam.periodical.commands.naming.CommandPath;
import ua.kiev.epam.periodical.commands.naming.SessionAttribute;
import ua.kiev.epam.periodical.entities.User;

/**
 * @author dmitriyg
 *
 */
public class LogoutCommand implements Command {

	private static final Logger logger = LoggerFactory
			.getLogger(LogoutCommand.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.command.Command#execute(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String login = ((User) request.getSession().getAttribute(
				SessionAttribute.USER)).getLogin();
		request.getSession().invalidate();
		response.sendRedirect(CommandPath.HOME);
		logger.info("User " + login + " has logged out.");
	}

}
