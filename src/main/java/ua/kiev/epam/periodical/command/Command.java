/**
 * 
 */
package ua.kiev.epam.periodical.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author dmitriyg
 *
 */
public interface Command {
	
	/**
	 * Should be implemented to perform some user action.
	 * @param request servlet request.
	 * @param response servlet response.
	 * @throws Exception possible if something went wrong.
	 */
	public void execute(HttpServletRequest request,
			HttpServletResponse response)throws Exception;
}
