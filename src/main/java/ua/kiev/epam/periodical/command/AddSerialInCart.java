/**
 * 
 */
package ua.kiev.epam.periodical.command;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kiev.epam.periodical.commands.naming.RequestAttribute;
import ua.kiev.epam.periodical.commands.naming.SessionAttribute;
import ua.kiev.epam.periodical.dao.DaoFactory;
import ua.kiev.epam.periodical.dao.SerialDao;
import ua.kiev.epam.periodical.entities.Serial;

/**
 * @author dmitriyg
 *
 */
public class AddSerialInCart implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try (SerialDao serialDao = DaoFactory.getFactory().getSerialDao();) {

			@SuppressWarnings("unchecked")
			LinkedList<Serial> cart = new LinkedList<>(
					((LinkedList<Serial>) request.getSession().getAttribute(
							SessionAttribute.CART)));
			String serialName = request
					.getParameter(RequestAttribute.SERIAL_NAME);
			Serial s = serialDao.findByName(serialName);
			cart.add(s);
			request.getSession().setAttribute(SessionAttribute.CART, cart);
		}
	}
}
