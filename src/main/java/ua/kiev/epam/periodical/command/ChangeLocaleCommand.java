/**
 * 
 */
package ua.kiev.epam.periodical.command;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kiev.epam.periodical.commands.naming.CommandPath;
import ua.kiev.epam.periodical.commands.naming.RequestAttribute;
import ua.kiev.epam.periodical.commands.naming.SessionAttribute;

/**
 * @author dmitriyg
 *
 */
public class ChangeLocaleCommand implements Command{
	
	/* (non-Javadoc)
	 * @see ua.kiev.epam.periodical.command.Command#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		 String newLocale = request.getParameter(RequestAttribute.NEW_LOCALE);
			request.getSession().setAttribute(SessionAttribute.LOCALE,
					new Locale(newLocale));
			response.sendRedirect(CommandPath.HOME);
	}

}
