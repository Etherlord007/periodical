/**
 * 
 */
package ua.kiev.epam.periodical.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kiev.epam.periodical.manager.ConfigurationManager;

/**
 * @author dmitriyg
 *
 */
public class HomeCommand implements Command{
	
	/* (non-Javadoc)
	 * @see ua.kiev.epam.periodical.command.Command#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		request.getRequestDispatcher(ConfigurationManager.getInstance()
	               .getProperty(ConfigurationManager.HOME_PAGE_PATH))
	               .forward(request, response);
	}

}
