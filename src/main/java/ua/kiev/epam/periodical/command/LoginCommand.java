/**
 * 
 */
package ua.kiev.epam.periodical.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.kiev.epam.periodical.commands.naming.CommandPath;
import ua.kiev.epam.periodical.commands.naming.RequestAttribute;
import ua.kiev.epam.periodical.commands.naming.SessionAttribute;
import ua.kiev.epam.periodical.dao.DaoFactory;
import ua.kiev.epam.periodical.dao.UserDao;
import ua.kiev.epam.periodical.entities.User;
import ua.kiev.epam.periodical.manager.ConfigurationManager;
import ua.kiev.epam.periodical.manager.MessageManager;

/**
 * @author dmitriyg
 *
 */
public class LoginCommand implements Command {

	private static final Logger logger = LoggerFactory
			.getLogger(LoginCommand.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.command.Command#execute(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try (UserDao userDao = DaoFactory.getFactory().getUserDao()){		
			String login = request.getParameter(RequestAttribute.LOGIN);
			String password = request.getParameter(RequestAttribute.PASSWORD);
			User user = userDao.getUser(login, password);
			if (user == null) {
				request.setAttribute(RequestAttribute.MESSAGE,
						MessageManager.LOGIN_ERROR_MESSAGE);
				request.setAttribute(RequestAttribute.ON_ERROR_GO_PATH,
						CommandPath.HOME);
				request.getRequestDispatcher(
						ConfigurationManager.getInstance().getProperty(
								ConfigurationManager.ERROR_PAGE_PATH)).forward(
						request, response);
				logger.info("Unsuccessfull login attempt with parameters: login="
						+ login + " password=" + password + ".");
			} else {
				request.getSession().setAttribute(SessionAttribute.USER, user);
				response.sendRedirect(CommandPath.HOME);
				logger.info("User " + user.getLogin()
						+ " logged in successfully.");
			}
		}
	}
}
