/**
 * 
 */
package ua.kiev.epam.periodical.manager;

import java.util.ResourceBundle;

/**
 * @author dmitriyg
 * This class encapsulates all application messages
 */
public class MessageManager {
	
	private static volatile MessageManager INSTANCE = new MessageManager();
    private ResourceBundle resourceBundle;
    /*Class get information from file messages.properties*/
    private static final String BUNDLE_NAME ="messages";
    
    /*keys to messages*/
     public static final String DATASOURCE_ERROR = "DATASOURCE_ERROR";
     public static final String LOGIN_ERROR_MESSAGE = "LOGIN_ERROR_MESSAGE";
     public static final String SERVLET_EXCEPTION_ERROR_MESSAGE = "SERVLET_EXCEPTION_ERROR_MESSAGE";
     public static final String IO_EXCEPTION_ERROR_MESSAGE="IO_EXCEPTION_ERROR_MESSAGE";
     public static final String PAGE_NOT_FOUND_ERROR = "PAGE_NOT_FOUND_ERROR";
     public static final String YOU_ARE_NOT_LOGGED_IN = "YOU_ARE_NOT_LOGGED_IN";
     public static final String YOUR_CART_IS_EMPTY = "YOUR_CART_IS_EMPTY";
    
     private MessageManager() {
        resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }
    /**
     * Gets MessageManager instance.
     * 
     * @return MessageManager instance
     */
    public static MessageManager getInstance() {
    	MessageManager localInstance = INSTANCE;
        if (localInstance == null) {
            synchronized (MessageManager.class) {
                localInstance = INSTANCE;
                if (localInstance == null) {
                	INSTANCE = localInstance = new MessageManager();
                }
            }
        }
        return localInstance;
    }
    
    /**
     * Returns text of specified message.
     * @param key
     *            name of message (use MessageManager static fields to observe
     *            message names).
     * @return specified message text.
     */
    public String getProperty(String key){
        return resourceBundle.getString(key);
    }
}
