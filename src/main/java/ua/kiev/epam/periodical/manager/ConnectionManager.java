/**
 * 
 */
package ua.kiev.epam.periodical.manager;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author dmitriyg
 * Class managing connections with data source.
 */
public class ConnectionManager {
	
	private static final Logger logger = LoggerFactory
			.getLogger(ConnectionManager.class);

	// configuration values to connect to data source
	private static final String JNDI_ENV = ConfigurationManager
			.getInstance().getProperty(ConfigurationManager.JNDI_ENV);
	private static final String DATASOURCE_PATH = ConfigurationManager
			.getInstance().getProperty(ConfigurationManager.DATASOURCE_PATH);

	// messages about possible errors
	private static final String DATASOURCE_ERROR = MessageManager.getInstance()
			.getProperty(MessageManager.DATASOURCE_ERROR);

	private static DataSource pool;

	private ConnectionManager() {
	}

	/**
	 * Initializes connection pool using JNDI. This method should be explicitly
	 * called before retrieving any connections.
	 * 
	 * @throws NamingException
	 *             if no resource is mapped to specified JNDI name or resource
	 *             is unavaliable.
	 */
	public static void initilize() throws NamingException {
		Context context = new InitialContext();
		pool = (DataSource) context.lookup(JNDI_ENV + DATASOURCE_PATH);
		logger.info("Connection pool initialized successfully.");
	}

	/**
	 * Gets connection to data source associated with this application.
	 * 
	 * @return connection to data source.
	 * @throws SQLException
	 *             if there's an error retrieving the connection.
	 */
	public static synchronized Connection getConnection() throws SQLException {
		if (pool == null) {
			throw new IllegalStateException(
					"ConnectionManager pool wasn't initialized (ConnectionManager.initialize() sould be called to do this).");
		}

		try {
			return pool.getConnection();
		} catch (SQLException e) {
			logger.error("Unable to get connection from pool.", e);
			throw new SQLException(DATASOURCE_ERROR, e);
		}
	}

	/**
	 * Frees connection.
	 * 
	 * @param connection
	 *            connection to free.
	 */
	public static synchronized void freeConnection(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			logger.warn("Unable to close connection.", e);
		}
	}
}
