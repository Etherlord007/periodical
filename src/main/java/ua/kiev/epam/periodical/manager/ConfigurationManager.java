/**
 * 
 */
package ua.kiev.epam.periodical.manager;

import java.util.ResourceBundle;

/**
 * @author dmitriyg
 *
 */
public class ConfigurationManager {
	
	private static volatile ConfigurationManager INSTANCE = new ConfigurationManager();
	
    private ResourceBundle resourceBundle;
    
    /*Class get information from file messages.properties*/
    private static final String BUNDLE_NAME = "config";
    public static final String DEFAULT_LANGUAGE = "DEFAULT_LANGUAGE";
    public static final String DEFAULT_ENCODING = "DEFAULT_ENCODING";
    public static final String ERROR_PAGE_PATH = "ERROR_PAGE_PATH";
    public static final String HOME_PAGE_PATH = "HOME_PAGE_PATH";
    public static final String JNDI_ENV = "JNDI_NAME";
    public static final String DATASOURCE_PATH = "DATASOURCE_PATH";
    public static final String SUBSCRIPTION_PAGE_PATH = "SUBSCRIPTION_PAGE_PATH";
    public static final String SHOP_PAGE_PATH = "SHOP_PAGE_PATH";
    
    private ConfigurationManager() {
		resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }
    
    /**
     * Gets ConfigurationMamager instance.
     *
     * @return ConfigurationMamager instance
     */
    public static ConfigurationManager getInstance() {
    		ConfigurationManager localInstance = INSTANCE;
            if (localInstance == null) {
                synchronized (ConfigurationManager.class) {
                    localInstance = INSTANCE;
                    if (localInstance == null) {
                    	INSTANCE = localInstance = new ConfigurationManager();
                    }
                }
            }
            return localInstance;
    }
    
    /**
     * Returns text of specified message.
     * @param key
     * 		name of message (use ConfigurationManager static fields to 
     *      observe message names).
     * @return specified message text.
     */
    public synchronized String getProperty(String key){
        return resourceBundle.getString(key);
    }
}
