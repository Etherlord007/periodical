/**
 * 
 */
package ua.kiev.epam.periodical.entities;

import java.io.Serializable;
import java.sql.Date;

import lombok.Data;

/**
 * @author dmitriyg Subscription entity
 */
@Data
public class Subscription implements Serializable {
	
	private static final long serialVersionUID = 3955277437285269264L;
	
	/**
	 * @param id
	 * @param totalSum
	 * @param dateOfSub
	 * @param userId
	 */
	public Subscription(Integer id, Double totalSum, Date dateOfSub,
			Integer userId) {
		super();
		this.id = id;
		this.totalSum = totalSum;
		this.dateOfSub = dateOfSub;
		this.userId = userId;
	}

	public Subscription() {

	}

	/**
	 * Subscription id
	 */
	private Integer id;
	
	/**
	 * Payment id
	 */
	private Double totalSum;
	
	/**
	 * Date of subscription
	 */
	private Date dateOfSub;
	
	/**
	 * User id
	 */
	private Integer userId;

}
