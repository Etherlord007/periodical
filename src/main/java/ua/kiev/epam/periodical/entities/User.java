/**
 * 
 */
package ua.kiev.epam.periodical.entities;

import java.io.Serializable;

import lombok.Data;

/**
 * @author dmitriyg
 *	User entity
 */
@Data
public class User implements Serializable{

	private static final long serialVersionUID = -5346496508872987628L;

	public User(){
		
	}
	/**
	 * @param id
	 * @param role
	 * @param login
	 * @param passwordHash
	 * @param subscriptionId
	 */
	public User(Integer id, String role, String login, String passwordHash) {
		super();
		this.id = id;
		this.role = role;
		this.login = login;
		this.passwordHash = passwordHash;
	}
	
	/**
	 * User id
	 */
	private Integer id;
	
	/**
	 * User role
	 */
	private String role;
	
	/**
	 * User login
	 */
	private String login;
	
	/**
	 * User password
	 */
	private String passwordHash;
	
	
}
