/**
 * 
 */
package ua.kiev.epam.periodical.entities;

import java.io.Serializable;

import lombok.Data;

/**
 * @author dmitriyg
 * Serial Entity
 */
@Data
public class Serial implements Serializable{
	
	private static final long serialVersionUID = -5878758370523260536L;

	public Serial(){
		
	}
	
	/**
	 * @param id
	 * @param name
	 * @param period
	 * @param details
	 * @param pricePerMonth
	 * @param image
	 */
	public Serial(Integer id, String name, Integer period, String details,
			Double pricePerMonth, String image) {
		super();
		this.id = id;
		this.name = name;
		this.period = period;
		this.details = details;
		this.pricePerMonth = pricePerMonth;
		this.image = image;
	}

	/**
	 * Serial id
	 */
	private Integer id;
	
	/**
	 * Serial name
	 */	
	private String name;
	
	/**
	 * Period of publishing in days
	 */
	private Integer period;
	
	/**
	 * Serial details
	 */
	private String details;
	
	/**
	 * Serial price per month
	 */
	private Double pricePerMonth;
	
	/**
	 * Path to image in file system
	 */
	private String image;
}
