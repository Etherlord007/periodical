/**
 * 
 */
package ua.kiev.epam.periodical.converters;

import java.sql.Date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

/**
 * @author dmitriyg
 *
 */
public class DateTimeConverter implements AttributeConverter<DateTime, Date> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.kiev.epam.periodical.converters.AttributeConverter#convertToDatabaseColumn
	 * (java.lang.Object)
	 */
	@Override
	public Date convertToDatabaseColumn(DateTime attribute) {
		return new Date(new LocalDateTime(attribute, DateTimeZone.UTC)
				.toDateTime().getMillis());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.kiev.epam.periodical.converters.AttributeConverter#
	 * convertToEntityAttribute(java.lang.Object)
	 */
	@Override
	public DateTime convertToEntityAttribute(Date dbData) {
		return new LocalDateTime(dbData).toDateTime(DateTimeZone.UTC);
	}

}
