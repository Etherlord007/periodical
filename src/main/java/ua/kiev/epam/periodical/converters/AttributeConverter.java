/**
 * 
 */
package ua.kiev.epam.periodical.converters;

/**
 * @author dmitriyg
 *
 */
public interface AttributeConverter<X,Y> {
	
	/**
	 * Converts the value stored in the entity attribute into the
	 * data representation to be stored in the database.
	 *
	 * @param attribute the entity attribute value to be converted
	 * @return the converted data to be stored in the database column
	 */
	public Y convertToDatabaseColumn (X attribute);
	
	/**
	 * Converts the data stored in the database column into the
	 * value to be stored in the entity attribute.
	 * 
	 * @param dbData the data from the database column to be converted
	 * @return the converted value to be stored in the entity attribute
	 */
	public X convertToEntityAttribute (Y dbData);
}
