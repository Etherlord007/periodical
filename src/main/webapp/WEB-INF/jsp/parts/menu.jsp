<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${locale}" scope="session" />
<fmt:setBundle basename="ua.kpi.locales.interface" />
<fmt:message key="HOME" var="home" />
<fmt:message key="MY_SUBSCRIPTION" var="my_subscription" />
<fmt:message key="SHOP" var="shop" />


<span class="menuitem"> <a href="home">${home}</a></span>
<span class="menuitem"> <a href="shop">${shop}</a></span>
<c:if test="${not empty user}">
<span сlass="menuitem"> <a href="my_subscription">${my_subscription}</a></span>
</c:if>