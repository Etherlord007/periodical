<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/taglibs/periodical.tld" prefix="md"%>



<fmt:setLocale value="${locale}" scope="session"/>

<span class="left-aligned"> <fmt:message key="HELLO_MESSAGE"/>:

	<span><a href="change_locale?new_locale=en"><img
			src="images/en_flag.png" alt="English" /> </a> </span> 
	<span><a href="change_locale?new_locale=ru"><img src="images/ru_flag.png"
			alt="Русский" width="30" height="18" /> </a> </span>
			
<c:choose>
	<c:when test="${empty user}">
		<form action="login" method="POST">
                    <table> 
                        <tr>
                            <td><fmt:message key="LOGIN" /></td>
                            <td>: <input type="text" name="login"></td>
                            <br/>
                        </tr>
                        <tr>
                            <td><fmt:message key="PASSWORD" /> </td>
                            <td>: <input type="password" name="password"> </td>
                        </tr>
                        <tr>
                            <td colspan=2>
                            <fmt:message key="LOGIN_BUTTON" var="login_button" />
                            <input type="submit" value="${login_button}">
                            </td>
                        </tr>
                    </table>
		</form>
	</c:when>
        <c:otherwise>
		<span><fmt:message key="ENTERED_AS" />: <span id="username">${user.login}</span>
		</span>
		<br />
		<fmt:message key="LOGOUT_BUTTON" var="logout_button" />
		<form action="logout" method="POST">
			<input type="submit" value="${logout_button}">
		</form>
	</c:otherwise>
</c:choose>