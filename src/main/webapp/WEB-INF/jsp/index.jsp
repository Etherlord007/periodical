<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:message key="HOME_PAGE_TITLE" var="title" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/styles.css" />
        <title>MotorDepot: ${title} </title>
    </head>
    <body>
        <c:import url="/WEB-INF/jsp/parts/header.jspf" />
        <div id="content">          
            <div id="identification_form">
                <c:import url="/WEB-INF/jsp/parts/locale_and_login.jsp" />
            </div>
            <div id="menustrip">
                <c:import url="/WEB-INF/jsp/parts/menu.jsp" />
            </div>
            <div class="statement">
                <p class="welcometext">
                    <fmt:message key="WELCOME_TEXT" />
                </p>
                <p>
                    <fmt:message key="ABOUT_TEXT" />
                </p>
            </div>
        </div>
        <c:import url="/WEB-INF/jsp/parts/footer.jspf" />
    </body>
</html>