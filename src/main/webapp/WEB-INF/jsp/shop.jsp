<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:import url="/WEB-INF/jsp/parts/header.jspf" />
	<div id="content">
		<div id="identification_form">
			<c:import url="/WEB-INF/jsp/parts/locale_and_login.jsp" />
		</div>
		<div id="menustrip">
				<c:import url="/WEB-INF/jsp/parts/menu.jsp" />
		</div>
		<form action="add_serial_in_cart" method="POST">
			<table>
				<c:forEach items ="${serials}" var="serial">
					<tr>
	      				<td><c:out value="${serial.name}" /></td>
	      				<td><c:out value="${serial.details}" /></td>
	      				<td><p>Price :</p><c:out value="${serial.pricePerMonth}" /></td>
	      				<form> 
							<input type="hidden" name="serial_name" value="${serial.name}"> 
						</form> 
	    			</tr>
				</c:forEach>
			</table>
		</form>
	</div>
	<c:import url="/WEB-INF/jsp/parts/header.jspf" />
</body>
</html>