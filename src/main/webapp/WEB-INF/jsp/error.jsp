<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${message}</title>
</head>
<body>
	<c:import url="/WEB-INF/jsp/parts/header.jspf" />
	<div id="content">
		<div id="registration_form">
			<c:import url="/WEB-INF/jsp/parts/locale_and_login.jsp" />
		</div>
		<div id="menustrip">
			<c:import url="/WEB-INF/jsp/parts/menu.jsp" />
		</div>
		<div class="statement">
			<div>${message}</div>
			<a href="${on_error_go_path}">Return</a>
		</div>
	</div>
	<c:import url="/WEB-INF/jsp/parts/footer.jspf" />
</body>
</html>